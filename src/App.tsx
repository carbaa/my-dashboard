import React, { useReducer } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";

import Clock from "./components/Clock";
import WeatherList from "./components/WeatherList";
import WeatherDisplay from "./components/WeatherDisplay";
import ConfigMenu from "./components/ConfigMenu";
import MessageDiv from "./components/utility/MessageDiv";

import { State, Action } from "./components/types/Reducer";

import "./App.css";

export default function App() {
  const reducer = (state: State, action: Action) => {
    switch (action.type) {
      case "isConfig":
        return { ...state, isConfig: action.payload };
      case "isClock":
        return { ...state, isClock: action.payload };
      case "isWeatherDisplay":
        return { ...state, isWeatherDisplay: action.payload };
      case "isWeatherList":
        return { ...state, isWeatherList: action.payload };
      case "isUpdating":
        return { ...state, isUpdating: action.payload };
      case "lastUpdated":
        return { ...state, lastUpdated: action.payload };
      case "isFinishedUpdating":
        if (state.error === "NetworkError") return state;

        //TODO: Error checking on the payload
        return {
          ...state,
          weatherData: action.payload,
          lastUpdated: Date.now(),
        };
      case "error":
        return { ...state, error: action.payload };
      default:
        return state;
    }
  };

  const initialState: State = {
    isConfig: false,
    isClock: true,
    isWeatherDisplay: true,
    isWeatherList: true,
    lastUpdated: null,
    isUpdating: false,
    weatherData: null,
    error: null,
    locationName: process.env.REACT_APP_LOCATION_NAME || "Seoul",
  };

  const [state, dispatch] = useReducer(reducer, initialState);

  const getWeather = async () => {
    dispatch({ type: "isUpdating", payload: true });

    //API URL must be defined

    if (!process.env.REACT_APP_API_URL) {
      dispatch({ type: "error", payload: "NetworkError" });
      return;
    }

    try {
      const response = await fetch(process.env.REACT_APP_API_URL, {
        method: "GET",
        mode: "cors",
      });
      const json = await response.json();

      dispatch({ type: "isFinishedUpdating", payload: json });
      dispatch({ type: "error", payload: null });
      dispatch({ type: "isUpdating", payload: false });
    } catch {
      dispatch({ type: "error", payload: "NetworkError" });
      dispatch({ type: "isUpdating", payload: false });
    }
  };

  if (state.lastUpdated === null && !state.isUpdating && state.error === null)
    getWeather();

  if (state.lastUpdated === null && state.error === "NetworkError") {
    return (
      <div className="App">
        <div className="App__container">
          <MessageDiv message="Network Error" />
        </div>
      </div>
    );
  }

  if (state.lastUpdated && state.weatherData) {
    const todayWeather = state.weatherData.current;
    const remainingWeather = state.weatherData.daily;
    const locationName = state.locationName;

    return (
      <div className="App">
        <div className="App__container">
          <button
            className="App__config-button"
            onClick={() => {
              dispatch({ type: "isConfig", payload: !state.isConfig });
            }}
          >
            <FontAwesomeIcon icon={faBars} />
          </button>
          {state.isConfig ? (
            <ConfigMenu dispatch={dispatch} state={state} />
          ) : null}
          {state.isClock ? <Clock /> : null}
          <div className="App__grid">
            {state.isWeatherDisplay ? (
              <WeatherDisplay
                todayWeather={todayWeather}
                location={locationName}
              />
            ) : null}
            {state.isWeatherList ? (
              <WeatherList data={remainingWeather} />
            ) : null}
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className="App">
      <div className="App__container">
        <MessageDiv message="Now Loading" />
      </div>
    </div>
  );
}
