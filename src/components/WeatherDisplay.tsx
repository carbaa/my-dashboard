import React from "react";
import "./WeatherDisplay.css";
import { Current } from "./types/API";
import WeatherIcon from "./utility/weather-icons/WeatherIcon";
import { epochDateInMD } from "./utility/Utility";

function WeatherDisplay(props: { todayWeather: Current; location: string }) {
  const { weather, temp, dt } = props.todayWeather;

  return (
    <div className="weather-display">
      <div className="weather-display__left-container">
        <div className="weather-display__icon">
          <WeatherIcon weatherCode={weather[0].main} />
        </div>
        <div className="weather-display__temperature-range">
          {Math.floor(temp)}°C
        </div>
      </div>

      <div className="weather-display__right-container">
        <div className="weather-display__location">{props.location}</div>
        <div className="weather-display__date">{epochDateInMD(dt)}</div>
      </div>
    </div>
  );
}

export default WeatherDisplay;
