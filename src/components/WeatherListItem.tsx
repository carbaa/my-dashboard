import React from "react";
import { Daily } from "./types/API";
import WeatherIcon from "./utility/weather-icons/WeatherIcon";
import "./WeatherListItem.css";
import { epochDateInMD } from "./utility/Utility";

function WeatherListItem(props: { data: Daily }) {
  const data = props.data;
  const { weather, temp, dt } = data;

  return (
    <div className="weather-list-item">
      <div className="weather-list-item__left-container">
        <div className="weather-list-item__location">{epochDateInMD(dt)}</div>
      </div>

      <div className="weather-list-item__right-container">
        <div className="weather-list-item__icon">
          <WeatherIcon weatherCode={weather[0].main} />
        </div>
        <div className="weather-list-item__temperature-range">
          {Math.floor(temp.min)}/{Math.floor(temp.max)}
        </div>
      </div>
    </div>
  );
}

export default WeatherListItem;
