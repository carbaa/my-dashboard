import React, { useState, useRef } from "react";
import "./Clock.css";

function Clock() {
  const [currentDate, setCurrentDate] = useState(new Date());
  const bgRef = useRef<HTMLDivElement>(null);

  setInterval(() => {
    setCurrentDate(new Date());
  }, 17);

  const minutesRatio =
    (currentDate.getSeconds() + currentDate.getMilliseconds() / 1000) / 60;

  if (bgRef.current !== null) {
    bgRef.current.style.width = `calc(${minutesRatio} * 100%)`;
  }

  const hh = `${currentDate.getHours()}`.padStart(2, "0");
  const mm = `${currentDate.getMinutes()}`.padStart(2, "0");
  const ss = `${currentDate.getSeconds()}`.padStart(2, "0");

  return (
    <div className="clock">
      <div className="bg" ref={bgRef}></div>
      <div className="text">
        {hh}:{mm}:{ss}
      </div>
    </div>
  );
}

export default Clock;
