import React from "react";
import "./ConfigMenu.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

import { State } from "./types/Reducer";
import StateButton from "./utility/StateButton";

export default function ConfigMenu(props: {
  dispatch: Function;
  state: State;
}) {
  const { isWeatherList, isWeatherDisplay, isClock } = props.state;
  const { dispatch } = props;

  return (
    <div className="config-menu">
      <div className="config-menu__close-button-container">
        <button
          className="config-menu__close-button"
          onClick={() => {
            dispatch({ type: "isConfig", payload: false });
          }}
        >
          <FontAwesomeIcon icon={faTimes} />
        </button>
      </div>
      <div className="config-menu__button-containers">
        <StateButton
          label="Show Clock"
          checked={isClock}
          onClick={(e) => {
            dispatch({ type: "isClock", payload: !isClock });
          }}
        />

        <StateButton
          label="Show Today's Weather"
          checked={isWeatherDisplay}
          onClick={(e) => {
            dispatch({
              type: "isWeatherDisplay",
              payload: !isWeatherDisplay,
            });
          }}
        />

        <StateButton
          label="Show Forecast"
          checked={isWeatherList}
          onClick={(e) => {
            dispatch({ type: "isWeatherList", payload: !isWeatherList });
          }}
        />
      </div>
    </div>
  );
}
