import React from "react";
import "./css/weather-icons.css";

const SnowIcon = () => <i className="wi wi-snow"></i>;
// const SleetIcon = () => <i className="wi wi-sleet"></i>;
// const HailIcon = () => <i className="wi wi-hail"></i>;
const ThunderStormIcon = () => <i className="wi wi-thunderstorm"></i>;
const HeavyRainIcon = () => <i className="wi wi-rain"></i>;
const LightRainIcon = () => <i className="wi wi-sprinkle"></i>;
// const ShowersIcon = () => <i className="wi wi-showers"></i>;
const HeavyCloudIcon = () => <i className="wi wi-cloudy"></i>;
// const LightCloudIcon = () => <i className="wi wi-day-cloudy"></i>;
const ClearIcon = () => <i className="wi wi-day-sunny"></i>;
const UnknownIcon = () => <i className="wi wi-alien"></i>;

type IconPropType = {
  size?: number;
  weatherCode: string;
};

export default function WeatherIcon(props: IconPropType) {
  const iconMapping: { [key: string]: Function } = {
    Thunderstorm: ThunderStormIcon,
    Drizzle: LightRainIcon,
    Rain: HeavyRainIcon,
    Snow: SnowIcon,
    Clear: ClearIcon,
    Clouds: HeavyCloudIcon,
  };

  const { size, weatherCode } = props;

  const Element = iconMapping[weatherCode] || UnknownIcon;

  return (
    <div style={{ fontSize: size || "24px" }}>
      <Element />
    </div>
  );
}

/*export {
  SnowIcon,
  SleetIcon,
  HailIcon,
  ThunderStormIcon,
  HeavyRainIcon,
  LightRainIcon,
  ShowersIcon,
  HeavyCloudIcon,
  LightCloudIcon,
  ClearIcon,
  UnknownIcon,
};*/
