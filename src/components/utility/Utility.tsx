export function epochDateInMD(epochSeconds: number): string {
  const dateObj = new Date(epochSeconds * 1000);

  return `${dateObj.getMonth() + 1}/${dateObj.getDate()}`;
}
