import React from "react";

export default function MessageDiv(props: { message: string }) {
  return (
    <div
      style={{
        height: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      {props.message}
    </div>
  );
}
