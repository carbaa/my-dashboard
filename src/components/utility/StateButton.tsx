import React from "react";
import "./StateButton.css";

export default function StateButton({
  label,
  checked,
  onClick,
}: {
  label: string;
  checked: boolean;
  onClick: React.MouseEventHandler<HTMLButtonElement>;
}) {
  const className = checked
    ? "state-button state-button--checked"
    : "state-button state-button--unchecked";

  return (
    <button onClick={onClick} className={className}>
      {label}
    </button>
  );
}
