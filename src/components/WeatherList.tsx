import React from "react";
import WeatherListItem from "./WeatherListItem";
import { Daily } from "./types/API";

function WeatherList(props: any) {
  const { data } = props;
  const WeatherListItemArray: Daily[] = data.map((day: Daily) => (
    <WeatherListItem data={day} />
  ));

  return <div className="weather-list">{WeatherListItemArray}</div>;
}

export default WeatherList;
