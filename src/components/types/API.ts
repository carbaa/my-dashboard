export type APIResponse = {
  lat: number;
  lon: number;
  timezone: string;
  timezone_offset: number;

  current: Current;
  daily: Daily[];
  alerts?: Alerts[];
};

export type Current = {
  dt: number;
  sunrise: number;
  sunset: number;
  temp: number;
  feels_like: number;
  pressure: number;
  humidity: number;
  dew_point: number;
  uvi: number;
  visibility: number;
  wind_speed: number;
  wind_gust?: number;
  wind_deg: number;
  weather: Weather[];
};

export type Weather = {
  id: number;
  main: string;
  description: string;
  icon: string;
};

export type Daily = {
  dt: number;
  sunrise: number;
  sunset: number;
  temp: Temp & { min: number; max: number };
  feels_like: Temp;
  pressure: number;
  humidity: number;
  dew_point: number;
  wind_speed: number;
  wind_gust?: number;
  wind_deg: number;
  clouds: number;
  pop: number;
  rain?: number;
  snow?: number;
  weather: Weather[];
};

export type Temp = {
  morn: number;
  day: number;
  eve: number;
  night: number;
};

type Alerts = {
  sender_name: string;
  event: string;
  start: number;
  end: number;
  description: string;
};
