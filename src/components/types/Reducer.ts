import { APIResponse } from "./API";

export type Action =
  | {
      type:
        | "isConfig"
        | "isClock"
        | "isWeatherDisplay"
        | "isWeatherList"
        | "isUpdating";
      payload: boolean;
    }
  | { type: "updateWeather"; payload?: any }
  | { type: "isFinishedUpdating"; payload: APIResponse }
  | { type: "lastUpdated"; payload: number }
  | { type: "weatherData"; payload: APIResponse }
  | { type: "error"; payload: null | "NetworkError" };

export type State = {
  isConfig: boolean;
  isClock: boolean;
  isWeatherDisplay: boolean;
  isWeatherList: boolean;
  lastUpdated: null | number;
  isUpdating: boolean;
  weatherData: null | APIResponse;
  error: null | "NetworkError";
  locationName: string;
};
